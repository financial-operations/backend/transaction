<?php

namespace App\Services;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Message\Response;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Client;

class NotificationService
{
    private $guzzleHttpclient;

    public function __construct(Client $guzzleHttpclient)
    {
        $this->guzzleHttpclient = $guzzleHttpclient;
    }

    public function sendNotification(string $verb, string $url, $token = null, $body = null)
    {
        $header = [
            'Content-Type'  => 'application/json',
            'Authorization' => 'Bearer '.$token,
        ];

        $response = $this->guzzleHttpclient->request($verb, $url,['header' => $header,\GuzzleHttp\RequestOptions::JSON => $body]);
        
        return json_decode($response->getBody()->getContents());
    }
}
