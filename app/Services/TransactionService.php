<?php

namespace App\Services;

use Illuminate\Http\Response;

use App\Repositories\Contracts\TransactionRepositoryInterface;
use App\Repositories\Contracts\TransactionTypeRepositoryInterface;

class TransactionService
{
    private $transactionRepository;
    private $transactionTypeRepository;
    private $guzzleService;
    private $notificationService;
    private $authorizeService;
    private $temporaryUserData;

    public function __construct(
        TransactionRepositoryInterface $transactionRepository,
        TransactionTypeRepositoryInterface $transactionTypeRepository,
        GuzzleService $guzzleService,
        \stdClass $temporaryUserData,
        NotificationService $notificationService,
        AuthorizeService $authorizeService)
    {
        $this->transactionRepository        = $transactionRepository;
        $this->transactionTypeRepository    = $transactionTypeRepository;
        $this->guzzleService                = $guzzleService;
        $this->authorizeService             = $authorizeService;
        $this->notificationService          = $notificationService;
        $this->temporaryUserData            = $temporaryUserData;
    }

    public function getTransactions()
    {
        return $this->transactionRepository->getAll();
    }

    /**
     * @param Request $requestDataTransaction
     */
    public function beginTransaction($requestDataTransaction)
    {
        try {
            $this->storeTemporaryUserData($requestDataTransaction);
            if (!$this->checkWalletBalance($this->temporaryUserData->profile['comum'])) {
                return ['mensage' => 'Saldo insuficiente!', 'error' => Response::HTTP_UNPROCESSABLE_ENTITY];
            }
            
            if (!$this->checkIfRegularUser()) {
                return ['mensage' => 'O perfil de usuário não pode realizar transações!', 'error' => Response::HTTP_UNPROCESSABLE_ENTITY];
            }
            
            $allowDeny = $this->authorizeService->allowDeny('get', env('AUTHORIZE_URL'));
            if ($allowDeny->message == 'Autorizado') {
                
                $this->creditWalletBalance();
                $this->debitWalletBalance();
                
                $this->notificationService->sendNotification('get', env('NOTIFICATION_URL'));
                
                return $this->endTransaction();
            }

            return ['mensage' => 'A transação não foi autorizada!', 'error' => Response::HTTP_UNPROCESSABLE_ENTITY];

        } catch (\Throwable $th) {
            $this->rollbackFailTransaction();
            return ['mensage' => 'Falha ao tentar efetuar a tranção!'];
        }
    }

    /**
     * @param Request $requestDataTransaction
     */
    private function storeTemporaryUserData($requestDataTransaction): void
    {
        $this->temporaryUserData->transaction['token']  = $requestDataTransaction->BearerToken();
        $this->temporaryUserData->transaction['amount'] = $requestDataTransaction->value;
        $this->temporaryUserData->profile = ['comum' => 'Comum', 'logista' => 'Logista'];

        $walletPayer = $this->guzzleService->makeConnection(
                                                            'get',
                                                            'transaction',
                                                            env('WALLET_URL').$requestDataTransaction->payer,
                                                            $this->temporaryUserData->transaction['token']
                                                        );
        
        $this->temporaryUserData->transaction['payer'] = $walletPayer;
        
        $walletPayee = $this->guzzleService->makeConnection(
                                                            'get',
                                                            'transaction',
                                                            env('WALLET_URL').$requestDataTransaction->payee,
                                                            $this->temporaryUserData->transaction['token']
                                                        );

        $this->temporaryUserData->transaction['payee'] = $walletPayee;
    }

    private function checkWalletBalance()
    {
        return $this->temporaryUserData->transaction['payer']->balance >= $this->temporaryUserData->transaction['amount'];
    }

    private function checkIfRegularUser(): Bool
    {
        $user =  $this->guzzleService->makeConnection(
                                                    'get',
                                                    'transaction',
                                                    env('SSO_URL').'usuario/'.$this->temporaryUserData->transaction['payer']->user_id,
                                                    $this->temporaryUserData->transaction['token']
                                                );

        return  $user->type_user == $this->temporaryUserData->profile['comum'];
    }

    private function debitWalletBalance()
    {
        $body = ['transaction_type' => 2, 'amount' => $this->temporaryUserData->transaction['amount']];
        return $this->guzzleService->makeConnection(
                                                    'put',
                                                    'transaction',
                                                    env('WALLET_URL').$this->temporaryUserData->transaction['payer']->user_id,
                                                    $this->temporaryUserData->transaction['token'],
                                                    $body
                                                );
    }

    private function creditWalletBalance()
    {
        $body = ['transaction_type' => 1, 'amount' => $this->temporaryUserData->transaction['amount']];
        return $this->guzzleService->makeConnection(
                                                    'put',
                                                    'transaction',
                                                    env('WALLET_URL').$this->temporaryUserData->transaction['payee']->user_id,
                                                    $this->temporaryUserData->transaction['token'],
                                                    $body
                                                );
    }

    private function rollbackFailTransaction()
    {
        $body = ['transaction_type' => 1, 'amount' => $this->temporaryUserData->transaction['amount']];
        return $this->guzzleService->makeConnection(
                                                    'put',
                                                    'transaction',
                                                    env('WALLET_URL').$this->temporaryUserData->transaction['payer']->user_id,
                                                    $this->temporaryUserData->transaction['token'],
                                                    $body
                                                );
    }

    public function getTransactionById(int $transactionId)
    {
        return $this->transactionRepository->findById($transactionId);
    }

    public function updateTransaction(array $dataTransaction, int $transactionId)
    {
        return $this->transactionRepository->update($dataTransaction, $transactionId);
    }

    public function removeTransaction(int $transactionId)
    {
        return $this->transactionRepository->delete($transactionId);
    }

    private function endTransaction()
    {   
        $transactions = [
            [
                'transaction_type_id' =>  2,
                'wallet_id' => $this->temporaryUserData->transaction['payer']->id,
                'payer' => $this->temporaryUserData->transaction['payer']->user_id,
                'payee' => $this->temporaryUserData->transaction['payee']->user_id,
                'amount' => $this->temporaryUserData->transaction['amount'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'transaction_type_id' =>  1,
                'wallet_id' => $this->temporaryUserData->transaction['payee']->id,
                'payer' => $this->temporaryUserData->transaction['payer']->user_id,
                'payee' => $this->temporaryUserData->transaction['payee']->user_id,
                'amount' => $this->temporaryUserData->transaction['amount'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];

        return $this->transactionRepository->add($transactions);
    }
}
