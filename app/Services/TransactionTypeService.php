<?php

namespace App\Services;

use App\Repositories\Contracts\TransactionTypeRepositoryInterface;

class TransactionTypeService
{
    private $formattedTypes;
    private $transactionTypeRepository;

    public function __construct(TransactionTypeRepositoryInterface $transactionTypeRepository)
    {
        $this->transactionTypeRepository = $transactionTypeRepository;
        $this->formattedTypes = [];
    }

    public function getTypes()
    {
        return $this->transactionTypeRepository->getAll();
    }

    public function newType(array $dataTypeTransaction)
    {
        return $this->transactionTypeRepository->add($this->formatTypes($dataTypeTransaction['types']));
    }

    public function getTypeById(int $transactionTypeId)
    {
        return $this->transactionTypeRepository->findById($transactionTypeId);
    }

    public function updateType(array $dataTypeTransaction, int $transactionTypeId)
    {
        return $this->transactionTypeRepository->update($dataTypeTransaction, intval($transactionTypeId));
    }

    public function removeType(int $transactionTypeId)
    {
        return $this->transactionTypeRepository->delete(intval($transactionTypeId));
    }

    private function formatTypes(array $dataTypeTransaction): array
    {
        foreach ($dataTypeTransaction as $key => $type) {
            array_push($this->formattedTypes, $type);
        }

        return $this->formattedTypes;
    }
}
