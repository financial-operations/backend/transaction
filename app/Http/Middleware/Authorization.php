<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Http\Response;

use App\Services\GuzzleService;

class Authorization
{
    private $guzzleService;

    public function __construct(GuzzleService $guzzleService)
    {
        $this->guzzleService = $guzzleService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->guzzleService->makeConnection('post', 'sso', env('SSO_URL').'checktoken', $request->BearerToken()) == Response::HTTP_OK) {
            return $next($request);
        } else {
            return response()->json(['erro' => 'Unauthorized access!'], Response::HTTP_NON_AUTHORITATIVE_INFORMATION);
        }
  
        return $next($request);
    }
}
