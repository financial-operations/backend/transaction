<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Services\TransactionTypeService;

class TransactionTypeController extends Controller
{
    private $transactionTypeService;

    public function __construct(TransactionTypeService $transactionTypeService)
    {
        $this->transactionTypeService = $transactionTypeService;
    }

    /**
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return response()->json($this->transactionTypeService->getTypes(), Response::HTTP_OK);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível recuperar os tipos das transações. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param int $transactionTypeId
     * @return Illuminate\Http\Response
     */
    public function show($transactionTypeId)
    {
        try {
            return response()->json($this->transactionTypeService->getTypeById($transactionTypeId), Response::HTTP_OK);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível recuperar o tipo de transação. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            return response()->json($this->transactionTypeService->newType($request->all()), Response::HTTP_CREATED);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível salvar o tipo de transação. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $transactionTypeId)
    {
        try {
            return response()->json($this->transactionTypeService->updateType($request->all(), $transactionTypeId), Response::HTTP_CREATED);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível atualizar o tipo de transação. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            return response()->json($this->transactionTypeService->removeType($request->id), Response::HTTP_OK);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível remover o tipo de transação. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}