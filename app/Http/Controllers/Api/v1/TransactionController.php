<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Services\TransactionService;

class TransactionController extends Controller
{
    private $transactionService;

    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    /**
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return response()->json($this->transactionService->getTransactions(), Response::HTTP_OK);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível recuperar as transações. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param int $transactionId
     * @return Illuminate\Http\Response
     */
    public function show($transactionId)
    {
        try {
            return response()->json($this->transactionService->getTransactionById($transactionId), Response::HTTP_OK);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível recuperar a transação. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            return response()->json($this->transactionService->beginTransaction($request), Response::HTTP_CREATED);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível realizar a transação. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response
     */
    public function update(WalletRequest $request, $transactionId)
    {
        try {
            return response()->json($this->transactionService->updateTransaction($request->all(), $transactionId), Response::HTTP_CREATED);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível realizar a transação. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Illuminate\Http\Request
     * @return Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            return response()->json($this->transactionService->removeTransaction($request->id), Response::HTTP_OK);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Uhm... isso não parece bom! Não foi possível remover a transação. Tente novamente mais tarde.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}