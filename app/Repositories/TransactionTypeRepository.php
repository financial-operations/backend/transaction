<?php 

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

use App\Repositories\Contracts\TransactionTypeRepositoryInterface;

class TransactionTypeRepository implements TransactionTypeRepositoryInterface
{
    public function getAll()
    {
        return  DB::table('transaction_type as ty')
                    ->select(
                        'ty.id',
                        'ty.type'
                    )
                    ->orderBy('ty.type', 'asc')
                    ->get();
    }

    public function add(array $dataTransactionType)
    {
        return  DB::table('transaction_type')->insert($dataTransactionType);
    }

    public function findById(int $transactionTypeId): Object
    {        
        return  DB::table('transaction_type as ty')
                    ->where('ty.id', '=', $transactionTypeId)
                    ->select(
                        'ty.id',
                        'ty.type'
                    )
                    ->get();
    }

    public function update(array $dataTransactionType, int $transactionTypeId): Bool
    {
        return  DB::table('transaction_type as ty')
                    ->where('ty.id', '=', $transactionTypeId)
                    ->update($dataTransactionType);
    }

    public function delete(int $transactionTypeId): Bool
    {
        return  DB::table('transaction_type as ty')
                    ->where('ty.id', '=', $transactionTypeId)
                    ->delete();
    }
}