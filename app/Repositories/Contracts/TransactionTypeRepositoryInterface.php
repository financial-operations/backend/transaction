<?php

namespace App\Repositories\Contracts;

interface TransactionTypeRepositoryInterface
{
    public function getAll();

    public function add(array $dataTransactionType);

    public function findById(int $transactionTypeId): Object;

    public function update(array $dataTransactionType, int $transactionTypeId): Bool;

    public function delete(int $transactionId): Bool;
}