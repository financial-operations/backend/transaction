<?php

namespace App\Repositories\Contracts;

interface TransactionRepositoryInterface
{
    public function getAll();

    public function add(array $dataTransaction);

    public function findById(int $transactionId): Object;

    public function update(array $dataTransaction, int $transactionId): Bool;

    public function delete(int $transactionId): Bool;
}