<?php 

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

use App\Repositories\Contracts\TransactionRepositoryInterface;

class TransactionRepository implements TransactionRepositoryInterface
{
    public function getAll()
    {
        return  DB::table('transaction as t')
                    ->join('transaction_type as ty', 't.transaction_type_id', '=', 'ty.id')
                    ->select(
                        't.id',
                        't.payer',
                        't.payee',
                        'ty.type',
                        't.amount',
                        DB::raw('DATE_FORMAT(t.created_at, "%d/%m/%Y %H:%i:%S") as created_at')
                    )
                    ->orderBy('t.id', 'desc')
                    ->get();
    }

    public function add(array $dataTransaction)
    {
        return DB::table('transaction')->insert($dataTransaction);
    }

    public function findById(int $transactionId): Object
    {        
        return  DB::table('transaction as t')
                    ->join('transaction_type as ty', 't.transaction_type_id', '=', 'ty.id')
                    ->where('t.id', '=', $transactionId)
                    ->select(
                        't.id',
                        't.payer',
                        't.payee',
                        'ty.type',
                        't.amount',
                        DB::raw('DATE_FORMAT(t.created_at, "%d/%m/%Y %H:%i:%S") as created_at')
                    )
                    ->get();
    }

    public function update(array $dataTransaction, int $transactionId): Bool
    {
        DB::transaction(function () use ($dataTransaction, $transactionId) {
            return  DB::table('transaction as t')
                        ->where('t.id', '=', $transactionId)
                        ->update($dataTransaction);
        });
    }

    public function delete(int $transactionId): Bool
    {
        DB::transaction(function () use ($transactionId) {
            return  DB::table('transaction as t')
                        ->where('t.id', '=', $transactionId)
                        ->delete();
        });
    }
}