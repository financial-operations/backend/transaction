# Microservice Transaction

Microsserviço responsável por realizar transações na carteira (saldo em conta) dos usuários.

### Settings:
- [Lumen Framework - Versão 7.2.1](https://lumen.laravel.com/docs/7.x)
- [PHP - Versão 7.4.4](https://www.php.net/downloads.php)

### Requirements:
- [Lumen Framework](https://lumen.laravel.com/docs/7.x/installation#server-requirements)

### Endpoint microservice Transaction.

- http://localhost:porta/v1/transaction


### Installation and Configuration:

- Clone projeto
        
        `git@gitlab.com:financial-operations/backend/transaction.git

- Imagem docker
        
        `registry.gitlab.com/financial-operations/backend/transaction`
        
### Executar somente o microservice Wallet.

- 1) Fazer o clone do projeto:

        `git@gitlab.com:financial-operations/backend/transaction.git`

- 2) Acessar o diretório transaction e executar os seguintes comandos e/ou configurações:
        
        configurar o arquivo .env
        Instalar dependências do projeto com: `composer install`

- 3) Subir a aplicação em um container docker (troque o nome home_user pelo diretório do projeto):
        
        `docker container run -dit --memory="256m" --name orq-transaction -h ms-transaction -v ~/home_user/transaction:/var/www -p 9001:80 registry.gitlab.com/financial-operations/backend/transaction
        && docker container exec orq-transaction service apache2 start && docker container exec orq-transaction service apache2 status`


