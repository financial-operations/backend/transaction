<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['middleware' => 'wallet-api', 'prefix' => 'v1', 'namespace' => 'Api\v1'], function () use ($router) {
    $router->get('transaction','TransactionController@index');
    $router->get('transaction/{transactionId}','TransactionController@show');
    $router->post('transaction','TransactionController@store');
    $router->put('transaction/{transactionId}','TransactionController@update');
    $router->delete('transaction','TransactionController@destroy');

    $router->get('transactiontype','TransactionTypeController@index');
    $router->get('transactiontype/{transactionTypeId}','TransactionTypeController@show');
    $router->post('transactiontype','TransactionTypeController@store');
    $router->put('transactiontype/{transactionTypeId}','TransactionTypeController@update');
    $router->delete('transactiontype','TransactionTypeController@destroy');
});
